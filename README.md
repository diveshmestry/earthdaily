# Earthdaily

## Name
Devops Engineer Hiring Homework

## Description
Earthdaily Event Driven SNS Notification. After provisioning, one can upload a file to the S3 bucket, it will trigger
a lambda function that reads the file and publishes the message in the file to an SNS topic.

## Usage
### Prerequisites
* Ensure you have terraform installed locally. 
* Ensure you have credentials configured to access the AWS account you want to deploy to

```
cd ./earthdaily
tf init
tf plan
tf apply
```

## Current Progress and Features
###Resources provisioned:
- S3 bucket
- SNS topic and SMS subscription
- Lambda function and python script to handle upload events and sns message publishing
- IAM role and permissions

###Features
- Python script for lambda function is a template that gets rendered with values needed,the sns topic arn is an example of such value.
- S3 Bucket provisioned
- Lambda deployed and triggers upon file upload to S3
- Logs of Event are stored in cloudwatch

## Todo
- [ ] Verify SMS Subscription
- [ ] Ensure only authorized users are able to upload files to specific S3 Bucket
- [ ] Build and test gitlab-ci.yml file and runner and test pipeline