variable "lambda_role_name" {
  default = ""
}

variable "lambda_iam_policy_name" {
  default = ""
}

variable "function_name" {
  default = ""
}
variable "handler_name" {
  default = ""
}
variable "runtime" {
  default = ""
}
variable "timeout" {
  default = ""
}
variable "environment" {
  default = ""
}

variable "email" {
  default = [""]
}