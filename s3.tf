resource "aws_s3_bucket" "bucket_1" {
  bucket = "earthdaily-input-bucket"
  force_destroy = true
  tags = {
    Name = "earthdaily-input-bucket"
  }
}