import json
import boto3
import urllib.parse
import sys
import logging
import traceback

logger = logging.getLogger()
logger.setLevel(logging.INFO)

print('Loading function')

s3 = boto3.client('s3')
sns = boto3.client('sns')


def lambda_handler(event, context):
    # print("Received event: " + json.dumps(event, indent=2))
    # logger.info(f'event: {event}')

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    try:
        #print(bucket)
        #print(key)
        print("getting file content")
        file_content = s3.get_object(Bucket=bucket, Key=key)["Body"].read().decode()
        topic = file_content.split(",")[0]
        message = file_content.split(",")[1]
        #print(message)
        #print(topic)

        #publish_message
        response = sns.publish(
            TargetArn="${topic_arn}",
            Message=json.dumps({'default': message}),
            MessageStructure='json'
        )
        return {
            'statusCode': 200,
            'body': json.dumps(response)
        }

    except Exception as e:
        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)