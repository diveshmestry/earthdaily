provider "aws" {
  allowed_account_ids = ["949427840810"]
  region              = "us-west-2"
  version             = "2.47.0"
}

provider "archive" {}

terraform {
  required_version = ">= 0.12.24"

  backend "s3" {
    bucket                  = "dmestry-terraform-bucket"
    key                     = "earthdaily/terraform.tfstate"
    region                  = "us-west-2"
    encrypt                 = true
    shared_credentials_file = "~/.aws/credentials"
  }
}
