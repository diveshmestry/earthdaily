
# specify variables for template file
data "template_file" "lambda_function" {
  template = file("${path.module}/template/welcome.tpl")
  vars  = {
    topic_arn = aws_sns_topic.earth_daily.arn
  }
}

# Render Templated Python Function
resource "local_file" "rendered_template" {
  content     = data.template_file.lambda_function.rendered
  filename 	= "${path.module}/rendered/welcome.py"
}

# Package Lambda Function Source Code in a ZIP Archive
data "archive_file" "lambda_s3_object_notification_zip" {
  type = "zip"
  output_path = "${path.module}/welcome.zip"
  source_dir = "${path.module}/rendered"
  depends_on =  [local_file.rendered_template]
}

# Creating Lambda IAM role
resource "aws_iam_role" "lambda_iam" {
  name = var.lambda_role_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy" "revoke_keys_role_policy" {
  name = var.lambda_iam_policy_name
  role = aws_iam_role.lambda_iam.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*",
        "logs:*",
        "cloudwatch:*",
        "sns:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# Creating Lambda resource
resource "aws_lambda_function" "function" {
  function_name    = var.function_name
  role             = aws_iam_role.lambda_iam.arn
  handler          = "${var.function_name}.lambda_handler"
  runtime          = var.runtime
  timeout          = var.timeout
  filename         = "welcome.zip"
  depends_on       = [data.archive_file.lambda_s3_object_notification_zip]
}


# Adding S3 bucket as trigger to my lambda and giving the permissions
resource "aws_s3_bucket_notification" "aws-lambda-trigger" {
  bucket = aws_s3_bucket.bucket_1.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.function.arn
    events              = ["s3:ObjectCreated:*"]

  }
}
resource "aws_lambda_permission" "test" {
  statement_id  = "AllowS3Invoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.function.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${aws_s3_bucket.bucket_1.id}"
}


