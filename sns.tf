locals {
  phones = var.email
}

resource "aws_sns_topic" "earth_daily" {
  name = "earthdaily-sns-topic"
}


resource "aws_sns_topic_subscription" "topic_email_subscription" {
  count     = length(local.phones)
  topic_arn = aws_sns_topic.earth_daily.arn
  protocol  = "sms"
  endpoint  = local.phones[count.index]
}